
#include <maya/MIOStream.h>
#include <vector>
#include <math.h>
#include <ctime>


#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MGlobal.h>
#include <maya/MQuaternion.h>



#include <maya/MPointArray.h>

using namespace std;

// #define SQ_LEN(a) ( (a.x)*(a.x) + (a.y)*(a.y) )

////////////////////////////////////////////////////////////////////////////////////

class Edge {

public:
				Edge() { a = b = l = r = -1; };
				~Edge() {};
	
public:
	int			a, b;
	int			l, r;
};

////////////////////////////////////////////////////////////////////////////////////

class Circle {

public:
				Circle() { radius = 0.0; };
				~Circle() {};
	bool		inside(MPoint& p);
	
	void		circumCircle(MPoint& a, MPoint& b, MPoint& c);
	
public:
	MPoint		center;
	double		radius;
};

////////////////////////////////////////////////////////////////////////////////////

class Triangulation {

public:
					Triangulation() { 
					// flatComponents.clear(); 
						points.clear(); 
						faceConnects.clear(); 
						nFaces = maxEdges = 0; 
					};
					~Triangulation() { edges.empty(); };
	void			findClosestNeighbours(int& u, int& v);
	void			completeFacet(int edgeIndex);
	void			updateLeftFace(int edgeIndex, int s, int t, int f);
	bool			addEdge(int a, int b, int l=-1, int r=-1);
	int				findEdge(int a, int b);
	void 			transformPoints();
	void 			unTransformPoints() ;
public:
	MVector			projection;
	int				maxEdges;
	int				nFaces;
	Circle			bC;
	MPointArray		points;
	MQuaternion		quat;
// 	MDoubleArray	flatComponents;
	MIntArray		faceConnects;
	vector<Edge>	edges;
};


////////////////////////////////////////////////////////////////////////////////////

class triangulator : public MPxNode {

public:
							triangulator();
	virtual					~triangulator();

	virtual	MStatus			compute( const MPlug& plug, MDataBlock& data);
	virtual	MObject			createMesh( Triangulation& tri, MObject& outData, int limit, MStatus& status );

	static	void*			creator();
	static	MStatus			initialize();
	
public:
	static	MObject			inputValue;
	static	MObject			projection;
	static	MObject			aInPoints;
	static	MObject			outputMesh;
	static	MTypeId			id;
};
