#include "triangulator.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>

#include <maya/MMatrix.h>
#include "jMayaIds.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

MTypeId		triangulator::id(k_triangulator);
MObject		triangulator::inputValue;
MObject		triangulator::projection;
MObject		triangulator::aInPoints;
MObject		triangulator::outputMesh;

double tripleCrossProduct (MPoint& a, MPoint& b, MPoint& c) {
	return ((b.x-a.x) * (c.y-a.y) - (b.y-a.y) * (c.x-a.x));
}

bool Circle::inside(MPoint& p) {
	
	MVector diff = center-MPoint(p.x,p.y,0.0);
	if (((diff.x * diff.x) + (diff.y * diff.y))  <= radius*radius)
		return 1;
	else
		return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Circle::circumCircle(MPoint& a, MPoint& b, MPoint& c ) {

	double cp = tripleCrossProduct(a, b, c);
	
	// if points are not aligned
	if (cp != 0.0) {
		double	aSq, bSq, cSq;
		double	num;
		double	cx, cy;

		aSq = a.x * a.x + a.y * a.y;
		bSq = b.x * b.x + b.y * b.y;
		cSq = c.x * c.x + c.y * c.y;
		num = aSq*(b.y - c.y) + bSq*(c.y - a.y) + cSq*(a.y - b.y);
		cx = num / (2.0 * cp);
		num = aSq*(c.x - b.x) + bSq*(a.x - c.x) + cSq*(b.x - a.x);
		cy = num / (2.0 * cp);
		center.x = cx;
		center.y = cy;
	}

	radius = MVector(center-MPoint(a.x,a.y,0.0)).length();
}


void Triangulation::findClosestNeighbours(int& u, int& v) {

	int	i, j;
	int				s = 0, t = 0;
	int				numPts = points.length();
	double			d;
	double			min = 9999999999999.0;		// need to find max value
	const double tol = 0.0000000001;
	for (i = 0; i < numPts-1; i++) {
		for (j = i+1; j < numPts; j++) {
			
			MPoint diff =  (points[i] - points[j]);
			d = (diff.x * diff.x) + (diff.y * diff.y);
			
			if( (d < min) && (d > tol)) {
				s = i;
				t = j;
				min = d;
			}
		}
	}
	

	u = s;
	v = t;

}


void Triangulation::transformPoints() {
	if (projection.isEquivalent(MVector::zero)){ 
		quat = MQuaternion();
	} else {
		projection.normalize();
		if ((projection * MVector::zAxis) > 0.0) {
			quat = MQuaternion(MVector(projection), MVector::zAxis);
		} else {
			quat = MQuaternion(MVector(projection), MVector::zNegAxis);
		}
		unsigned len =points.length();
		for (unsigned i = 0; i < len; i++) {
			MPoint & p = points[i];
			p = MVector(p).rotateBy(quat);
		}
	}
}

void Triangulation::unTransformPoints() {
	unsigned len =points.length();
	for (unsigned i = 0; i < len; i++) {
		MPoint & p = points[i];
		p = MVector(p).rotateBy(quat.inverse());
	}
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool Triangulation::addEdge(int a, int b, int l, int r) {
	
	Edge e;

	// if edge doesn't already exist
	if (findEdge(a,b) == -1) {
		if (a<b) {
			e.a = a;
			e.b = b;
			e.l = l;
			e.r = r;
			edges.push_back(e);
		} else {
			e.a = b;
			e.b = a;
			e.l = r;
			e.r = l;
			edges.push_back(e);
		}
		return 1;
	}
	
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

int Triangulation::findEdge(int a, int b) {
	
	 unsigned int	i=0;
	
	 for (std::vector<Edge>::const_iterator iter = edges.begin(); iter != edges.end();iter++, i++) 
	 {
	 		if	(	((*iter).a == a && (*iter).b == b) || 	((*iter).a == b && (*iter).b == a) )
			return i;
	 }
	return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Triangulation::updateLeftFace(int eI, int s, int t, int f) {

	if (!((edges.at(eI).a == s && edges.at(eI).b == t) || (edges.at(eI).a == t && edges.at(eI).b == s)))
		return;
		
	if (edges.at(eI).a == s && edges.at(eI).l == -1)
		edges.at(eI).l = f;
	else if (edges.at(eI).b == s && edges.at(eI).r == -1)
		edges.at(eI).r = f;
	else
		return;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Triangulation::completeFacet(int edgeIndex) {

	 int		s, t;
	 unsigned u;
	
	

	// Check which side needs completion on edge
	if (edges.at(edgeIndex).l == -1) {
		s = edges.at(edgeIndex).a;
		t = edges.at(edgeIndex).b;
	} else if (edges.at(edgeIndex).r == -1) {
		s = edges.at(edgeIndex).b;
		t = edges.at(edgeIndex).a;
	} else {
		return;
	}

	// Find a point on left of edge
	for (u = 0; u < points.length(); u++) {
		if (u == s || u == t)
			continue;
		if (tripleCrossProduct(points[s], points[t], points[u]) > 0.0)
			break;
	}

	// Find best point on left of edge
	unsigned uIndex = u;
	if (uIndex < points.length()) {

		bC.circumCircle(points[s], points[t], points[uIndex]);

		for (u = uIndex+1; u < points.length(); u++) {
			if (u == s || u == t)
				continue;

			if (tripleCrossProduct(points[s], points[t], points[u]) > 0.0) {
				if (bC.inside(points[u])) {
					uIndex = u;
					bC.circumCircle(points[s], points[t], points[u]);
				}
			}
		}
	}

	// Add new triangle or update edge info if s,t is on hull
	if (uIndex < points.length()) {
		// Update face information of edge being completed.
		updateLeftFace(edgeIndex, s, t, nFaces);
		faceConnects.append(s);
		faceConnects.append(t);
		faceConnects.append(uIndex);
		nFaces++;

		// Add new edge or update face info of old edge.
		edgeIndex = findEdge(uIndex, s);
		if (edgeIndex == -1)
			edgeIndex = addEdge(uIndex, s, nFaces, -1);	// New edge
		else
			updateLeftFace(edgeIndex, uIndex, s, nFaces);	// Old edge

		// Add new edge or update face info of old edge.
		edgeIndex = findEdge(t, uIndex);
		if (edgeIndex == -1)
			edgeIndex = addEdge(t, uIndex, nFaces, -1);	// New edge
		else
			updateLeftFace(edgeIndex, t, uIndex, nFaces);	// Old edge
	} else {
		updateLeftFace(edgeIndex, s, t, 0);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

triangulator::triangulator () {
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

triangulator::~triangulator () {
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void* triangulator::creator () {
	return new triangulator();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

MStatus triangulator::initialize () {

	MFnNumericAttribute	nAttr;
	MFnTypedAttribute	tAttr;
	MFnMatrixAttribute		mAttr;
	// input
	inputValue = nAttr.create( "limit", "lmt", MFnNumericData::kInt );
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setWritable(true);
	nAttr.setKeyable(true);
	nAttr.setHidden(false);
	nAttr.setMin(-1);
	nAttr.setSoftMin(-1);
	nAttr.setSoftMax(256);
	nAttr.setDefault(-1);
	addAttribute(inputValue);

	projection = nAttr.create( "projection", "pr", MFnNumericData::k3Double );
	nAttr.setHidden(false);
	nAttr.setKeyable(true);
	nAttr.setDefault( 0.0,  0.0, 1.0);
	addAttribute(projection);	
	
   // inputCloud = tAttr.create( "inPoints", "inp", MFnData::kVectorArray);
   // tAttr.setStorable(false);
   // tAttr.setReadable(false);
   // tAttr.setWritable(true);
   // addAttribute(inputCloud);
	
	
	///////////////////////////////////////////////////////////////////////
	aInPoints = mAttr.create("inTransforms","itfs");
	mAttr.setReadable( false );
	mAttr.setStorable( true );
	mAttr.setArray( true );
	addAttribute( aInPoints );
	///////////////////////////////////////////////////////////////////////
   

	
	
	// output
	outputMesh = tAttr.create( "outMesh", "outm", MFnData::kMesh );
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	tAttr.setWritable(false);
	addAttribute(outputMesh);

	attributeAffects (inputValue, outputMesh);
	attributeAffects (projection, outputMesh);
	attributeAffects (aInPoints, outputMesh);

	return MS::kSuccess;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

MObject triangulator::createMesh (Triangulation& tri, MObject& outData, int limit, MStatus& status) {

	 unsigned 	i = 0;
	int				a, b;
		
	
	// TRIANGULATION

	// rotate the points to align with the projection vector
	
//	clock_t time_a = clock();
	tri.transformPoints();
//	clock_t time_b = clock();
//	clock_t transformPointsTime = time_b - time_a;
	
	tri.findClosestNeighbours(a, b);
//	clock_t time_c = clock();
//	clock_t findClosestNeighboursTime = time_c - time_b;
	
	// Add edge to triangulation
	tri.addEdge(a, b);
//	clock_t time_d = clock();
//	clock_t addEdgeTime =time_d - time_c;
	
	// Complete loose edges
	
	while ( i < tri.edges.size()) {
		if (limit!=-1 && limit<(tri.nFaces-1))
			break;
		if (tri.edges.at(i).l == -1) 	tri.completeFacet(i);
		if (tri.edges.at(i).r == -1) 	tri.completeFacet(i);
		i++;
	}
//	clock_t time_e = clock();
//	clock_t completeFacetsTime = time_e -time_d ;

	// Reset the points coordinates to their initial values
	tri.unTransformPoints();
//	clock_t time_f = clock();
//	clock_t unTransformPointsTime = time_f -time_e ;

	// MESH BUILDING
	int		numVertices = tri.points.length();
	MFnMesh	meshFS;
	MObject meshObject;

	// Set up and array to describe the number of vertices per facet
	MIntArray faceCounts(tri.nFaces, 3);
	
	meshObject = meshFS.create(	numVertices, tri.nFaces,	tri.points, faceCounts, tri.faceConnects,	outData, &status);
	// Make hard edges
	meshFS.setObject(meshObject);
	for (int i=0; i<meshFS.numEdges(); i++)
		meshFS.setEdgeSmoothing(i, 0);
	meshFS.cleanupEdgeSmoothing();
//	clock_t time_g = clock();
//	clock_t makeMeshTime = time_g -time_f ;

//	clock_t totalTime = time_g -time_a ;

  //  cerr << "transformPointsTime: " << transformPointsTime << "(" << ((transformPointsTime / totalTime)*100) << ")"<<endl;
  //  cerr << "findClosestNeighboursTime: " << findClosestNeighboursTime << "(" << ((findClosestNeighboursTime / totalTime)*100) << ")"<<endl;
  //  cerr << "addEdgeTime: " << addEdgeTime << "(" << ((addEdgeTime / totalTime)*100) << ")"<<endl;
  //  cerr << "completeFacetsTime: " << completeFacetsTime << "(" << ((completeFacetsTime / totalTime)*100) << ")"<<endl;
  //  cerr << "unTransformPointsTime: " << unTransformPointsTime << "(" << ((unTransformPointsTime / totalTime)*100) << ")"<<endl;
  //  cerr << "makeMeshTime: " << makeMeshTime << "(" << ((makeMeshTime / totalTime)*100) << ")"<<endl;
	
	


	return meshObject;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

MStatus triangulator::compute(const MPlug& plug, MDataBlock& data) {

	MStatus	status;
	
	if (plug == outputMesh) {

		register unsigned  int i;

		// get limit
		int limit = data.inputValue( inputValue ).asInt();
		
		Triangulation tri;
		
		MArrayDataHandle hInPoints = data.inputArrayValue( aInPoints, &status );
		unsigned int count = hInPoints.elementCount(&status); 
		
		
	 if (!count) {return MS::kUnknownParameter;}
    
	 unsigned el = 0;
	 do { 
    
	 	if (status == MS::kSuccess) {
	 		MDataHandle hPt = hInPoints.inputValue( &status );	
	 		MMatrix mat(hPt.asMatrix());
	 		MPoint pt = MPoint(mat[3][0],mat[3][1],mat[3][2]);
	 		tri.points.append(pt);
	 		el++;
	 	}
	 } while (hInPoints.next() == MS::kSuccess );
		
//	cerr << "count: " << count << endl;
		
   //  for(unsigned i = 0;i < count; i++) {
   //	if( hInPoints.next() == MS::kSuccess) {
   //	//	cerr << "adding a point: "<< i << endl;
   //  		MDataHandle hPt = hInPoints.inputValue(&status ); 
   //		MMatrix mat(hPt.asMatrix());
   //		MPoint pt = MPoint(mat[3][0],mat[3][1],mat[3][2]);
   //	  	tri.points.append(MPoint(pt));
   //	}
   //  }
//	cerr << "tri.points" << tri.points << endl;

		tri.projection = data.inputValue( projection ).asVector();
		
		
		// output data
		MFnMeshData dataCreator;
		MObject newOutputData = dataCreator.create(&status);
		MDataHandle outputHandle = data.outputValue(outputMesh, &status);
	
		// build mesh
		MObject	newMesh = createMesh(tri, newOutputData, limit, status);
		
		if (!status) newOutputData = MObject::kNullObj;

		// write plug
		outputHandle.set(newOutputData);
		data.setClean( plug );
	
	} else {    
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}

