#include <maya/MIOStream.h>
#include <math.h>
//#include <vector.h>

#include <maya/MPxNode.h>

#include <maya/MFnTypedAttribute.h>

#include <maya/MIntArray.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
// #include <maya/MFnNurbsSurface.h>
// #include <maya/MFnNurbsSurfaceData.h>
#include <maya/MPointArray.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFloatPointArray.h>
#include <maya/MObjectArray.h>

#include <maya/MTypeId.h> 
#include <maya/MPlug.h> 
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 

#include "errorMacros.h"
#include "jMayaIds.h"

#include "shrinkwrap.h" 

#define kTOLERANCE 0.0001

MTypeId shrinkWrap::id( k_shrinkWrap  );

MObject shrinkWrap::aOffset;  
MObject shrinkWrap::aFromMesh;  
MObject shrinkWrap::aToMesh;  
MObject shrinkWrap::aInMesh; 	 
MObject shrinkWrap::aOutMesh;

shrinkWrap::shrinkWrap() {}
shrinkWrap::~shrinkWrap() {}

void* shrinkWrap::creator()
{
	return new shrinkWrap();
}

MStatus shrinkWrap::compute( const MPlug& plug, MDataBlock& data )	{
	
	MStatus st;
 	MString method("shrinkWrap::compute");
	
	if(!( plug == aOutMesh )) return MS::kUnknownParameter;
	
	////////////////////////////////////////////////////
	MObject fromMesh =  data.inputValue(aFromMesh).asMesh();
	MObject toMesh =  data.inputValue(aToMesh).asMesh();
	MObject inMesh =  data.inputValue(aInMesh).asMesh();

	MFnMesh mFnFrom(fromMesh);
	MFnMesh mFnTo(toMesh);
	MFnMesh mFnIn(inMesh);

	int numVerts = mFnFrom.numVertices();

	if (numVerts != mFnTo.numVertices()) return MS::kUnknownParameter;
	
	MMeshIsectAccelParams ap = mFnIn.autoUniformGridParams();
	
	MFloatPointArray vertsFrom, vertsTo;
	st = mFnFrom.getPoints(vertsFrom,MSpace::kWorld);ert;
	st = mFnTo.getPoints(vertsTo,MSpace::kWorld);ert;
	
	MFloatPoint fpSource;
	MFloatVector fvRayDir;
	float hitRayParam, defaultDist;
	MFloatPointArray hitPoints(numVerts);

	const float & offset = data.inputValue(aOffset).asFloat();

	for (int i = 0;i<numVerts;i++) {
		fpSource = vertsFrom[i];
		MFloatPoint & hitPoint =  hitPoints[i];

		// Fire a ray from the fromPoint towards the toPoint.
		// If the ray hits the inMesh on the way, we use that 
		// hitPoint, otherwise we use the toPoint.
		// We always add the offset.
		if (vertsTo[i].isEquivalent(vertsFrom[i])) {
			hitPoint = vertsTo[i];
		} else {
			fvRayDir = (vertsTo[i] - vertsFrom[i]);
			defaultDist = fvRayDir.length();
			fvRayDir.normalize();
			bool hit = mFnIn.closestIntersection(
			fpSource, fvRayDir, NULL, NULL, false, 
			MSpace::kWorld, 99999,0, &ap, hitPoint, 
			&hitRayParam, NULL, NULL, NULL, NULL, 0.000001f, &st); er;
			if ((!hit) || (defaultDist < hitRayParam)) hitPoint = vertsTo[i];
		}
		hitPoint += (-fvRayDir * offset);
	}
	
	MFnMesh newMeshFn;
	MFnMeshData dataCreator;
	MObject outMeshData = dataCreator.create( &st ); er;
	MObject newMesh = newMeshFn.copy(fromMesh,outMeshData, &st); ert;
	

	newMeshFn.setPoints(hitPoints,MSpace::kObject);

	// output
	////////////////////////////////////////////////////
	MDataHandle hOut = data.outputValue(aOutMesh);
	hOut.set(outMeshData);
	st = data.setClean( plug );er;
	////////////////////////////////////////////////////
	
	return MS::kSuccess;
}

MStatus shrinkWrap::initialize()
{
	
	MStatus st;
 	MString method("shrinkWrap::initialize");
	
	MFnTypedAttribute   tAttr;
	MFnNumericAttribute   nAttr;
	
	aFromMesh = tAttr.create( "fromMesh", "frm", MFnData::kMesh, &st );er
	tAttr.setReadable(false);
	st = addAttribute(aFromMesh);	er;
	
	aToMesh = tAttr.create( "toMesh", "tom", MFnData::kMesh, &st );er
	tAttr.setReadable(false);
	st = addAttribute(aToMesh);	er;
	
	aInMesh = tAttr.create( "inMesh", "inm", MFnData::kMesh, &st );er
	tAttr.setReadable(false);
	st = addAttribute(aInMesh);	er;
	
	aOffset =  nAttr.create( "offset", "off", MFnNumericData::kFloat, 0.0f, &st ); er;
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
 	nAttr.setDefault(0.0f);
 	st = addAttribute(aOffset);	er;
	
	aOutMesh = tAttr.create( "outMesh", "out", MFnData::kMesh, &st );er
	tAttr.setReadable(true);
	st = addAttribute(aOutMesh); er;
	
	st = attributeAffects(aOffset, aOutMesh );
	st = attributeAffects(aInMesh, aOutMesh );
	st = attributeAffects(aToMesh, aOutMesh );
	st = attributeAffects(aFromMesh, aOutMesh );
	
	return MS::kSuccess;
}
