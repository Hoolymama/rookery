 #include <maya/MIOStream.h>
#include <math.h>
//#include <vector.h>

#include <maya/MPxNode.h>

#include <maya/MFnTypedAttribute.h>

#include <maya/MIntArray.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MPointArray.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFloatPointArray.h>
#include <maya/MObjectArray.h>

#include <maya/MMeshIntersector.h> 
#include <maya/MDagPath.h> 

#include <maya/MTypeId.h> 
#include <maya/MPlug.h> 
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 

#include "errorMacros.h"
#include "jMayaIds.h"

#include "curve_shrink_wrap.h" 

#define kTOLERANCE 0.0001

MTypeId curveShrinkWrap::id( k_curveShrinkWrap  );

MObject curveShrinkWrap::aOffset;  
MObject curveShrinkWrap::aFromCurve;  
MObject curveShrinkWrap::aToCurve;  
MObject curveShrinkWrap::aInMesh; 	 
MObject curveShrinkWrap::aOutCurve;
MObject curveShrinkWrap::aUseClosestPoint;
curveShrinkWrap::curveShrinkWrap() {}
curveShrinkWrap::~curveShrinkWrap() {}

void* curveShrinkWrap::creator()
{
	return new curveShrinkWrap();
}

MStatus curveShrinkWrap::compute( const MPlug& plug, MDataBlock& data )	{
	
	MStatus st;
 	MString method("curveShrinkWrap::compute");
	
	if(!( plug == aOutCurve )) return MS::kUnknownParameter;
	bool useClosest = data.inputValue(aUseClosestPoint).asBool();
	const float & offset = data.inputValue(aOffset).asFloat();
	
	////////////////////////////////////////////////////
	MObject fromCurve, toCurve;
	MFnNurbsCurve fnFrom, fnTo;
	MPointArray vertsFrom, vertsTo; 


	fromCurve =  data.inputValue(aFromCurve).asNurbsCurve();
	fnFrom.setObject(fromCurve);
	int numCVs = fnFrom.numCVs() ;
	st = fnFrom.getCVs(vertsFrom,MSpace::kWorld);er;

	MObject inMesh =  data.inputValue(aInMesh).asMeshTransformed();
	MFnMesh mFnIn(inMesh);



	MPointArray hitPoints(numCVs);

	if (! useClosest){
		toCurve =  data.inputValue(aToCurve).asNurbsCurve();
		fnTo.setObject(toCurve);
		st = fnTo.getCVs(vertsTo,MSpace::kWorld);
		if (st.error()) useClosest = true;
		if (numCVs != fnTo.numCVs()) useClosest = true;
	}

	if (useClosest){

		MMeshIntersector intersector;
		MPointOnMesh pointInfo;

		st = intersector.create( inMesh ); er;

		for (int i = 0;i<numCVs;i++) {
			MPoint & hitPoint =  hitPoints[i];
			

			
       		MPoint &searchPoint = vertsFrom[i];



			st = intersector.getClosestPoint( searchPoint,pointInfo );
			hitPoint = pointInfo.getPoint();
			
			if (offset != 0.0) {
				MVector diffN = (searchPoint-hitPoint).normal();
				int sign = ( (pointInfo.getNormal() * diffN) > 0) ? 1 : -1 ;
				hitPoint += diffN * double(offset * sign);
			}
		}
	} else { // shrink using both fromCurve and toCurve
		MMeshIsectAccelParams ap = mFnIn.autoUniformGridParams();
		MFloatPoint fpSource;
		MFloatVector fvRayDir;
		float hitRayParam, defaultDist;

		for (int i = 0;i<numCVs;i++) {
			fpSource = MFloatPoint(vertsFrom[i]);
			MPoint & hitPoint =  hitPoints[i];


			// Fire a ray from the fromPoint towards the toPoint.
			// If the ray hits the inMesh on the way, we use that 
			// hitPoint, otherwise we use the toPoint.
			// We always add the offset.
			if (vertsTo[i].isEquivalent(vertsFrom[i])) {
				hitPoint = vertsTo[i];
			} else {
				MFloatPoint fHitPoint;
				fvRayDir = MFloatVector(vertsTo[i] - vertsFrom[i]);
				defaultDist = fvRayDir.length();
				fvRayDir.normalize();
				bool hit = mFnIn.closestIntersection(
					fpSource, fvRayDir, NULL, NULL, false, 
					MSpace::kWorld, 99999,0, &ap, fHitPoint, 
					&hitRayParam, NULL, NULL, NULL, NULL, 0.000001f, &st); er;

				if ((!hit) || (defaultDist < hitRayParam)) {
					hitPoint = MFloatPoint(vertsTo[i]);
				} else {
					hitPoint = MPoint(fHitPoint);
				}
			}
			hitPoint += MPoint(-fvRayDir * offset);
		}

	}
	

	MFnNurbsCurve newCurveFn;
	MFnNurbsCurveData dataCreator;
	MObject outCurveData = dataCreator.create( &st ); er;
	MObject newCurve = newCurveFn.copy(fromCurve,outCurveData, &st); er;
	

	newCurveFn.setCVs(hitPoints,MSpace::kObject);

	// output
	////////////////////////////////////////////////////
	MDataHandle hOut = data.outputValue(aOutCurve);
	hOut.set(outCurveData);
	st = data.setClean( plug );er;
	////////////////////////////////////////////////////
	
	return MS::kSuccess;
}

MStatus curveShrinkWrap::initialize()
{
	
	MStatus st;
 	MString method("curveShrinkWrap::initialize");
	
	MFnTypedAttribute   tAttr;
	MFnNumericAttribute   nAttr;

	aUseClosestPoint = nAttr.create( "useClosestPoint", "ucp", MFnNumericData::kBoolean);er
	nAttr.setReadable(false);
	nAttr.setDefault(false);
	st = addAttribute(aUseClosestPoint);er;
	
	aFromCurve = tAttr.create( "fromCurve", "frc", MFnData::kNurbsCurve, &st );er
	tAttr.setReadable(false);
	st = addAttribute(aFromCurve);	er;
	
	aToCurve = tAttr.create( "toCurve", "toc", MFnData::kNurbsCurve, &st );er
	tAttr.setReadable(false);
	st = addAttribute(aToCurve);	er;
	
	aInMesh = tAttr.create( "inMesh", "inm", MFnData::kMesh, &st );er
	tAttr.setReadable(false);
	st = addAttribute(aInMesh);	er;
	
	aOffset =  nAttr.create( "offset", "off", MFnNumericData::kFloat, 0.0f, &st ); er;
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
 	nAttr.setDefault(0.0f);
 	st = addAttribute(aOffset);	er;
	
	aOutCurve = tAttr.create( "outCurve", "out", MFnData::kNurbsCurve, &st );er
	tAttr.setReadable(true);
	st = addAttribute(aOutCurve); er;
	
	st = attributeAffects(aOffset, aOutCurve );
	st = attributeAffects(aInMesh, aOutCurve );
	st = attributeAffects(aToCurve, aOutCurve );
	st = attributeAffects(aFromCurve, aOutCurve );
	st = attributeAffects(aUseClosestPoint, aOutCurve );
	
	return MS::kSuccess;
}
