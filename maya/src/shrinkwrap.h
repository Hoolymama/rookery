/*
 *  shrinkWrap.h
 *  jtools
 *
 *  Created by jmann on 10/29/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _shrinkWrap
#define _shrinkWrap

#include <maya/MObject.h>
#include <maya/MGlobal.h>
#include <maya/MDataBlock.h>


class shrinkWrap : public MPxNode
	{
	public:
		shrinkWrap();
		virtual				~shrinkWrap(); 
		
		virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
		static  void*		creator();
		static  MStatus		initialize();
		
	public:
		
		static  MObject     aOffset;  
		static  MObject     aFromMesh;  
		static  MObject     aToMesh;  
		static  MObject     aInMesh;  
		
		
		
		static  MObject     aOutMesh;      
		static	MTypeId		id;          
		
	private:
		
	};

#endif

