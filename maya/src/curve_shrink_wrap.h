/*
 *  curveShrinkWrap.h
 *  jtools
 *
 *  Created by jmann on 10/29/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _curveShrinkWrap
#define _curveShrinkWrap

#include <maya/MObject.h>
#include <maya/MGlobal.h>
#include <maya/MDataBlock.h>


class curveShrinkWrap : public MPxNode
	{
	public:
		curveShrinkWrap();
		virtual				~curveShrinkWrap(); 
		
		virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
		static  void*		creator();
		static  MStatus		initialize();
		
	public:
		
		static  MObject     aUseClosestPoint;  
		static  MObject     aOffset;  
		static  MObject     aFromCurve;  
		static  MObject     aToCurve;  
		static  MObject     aInMesh;  
			
		
		static  MObject     aOutCurve;      
		static	MTypeId		id;          
		
	private:
		
	};

#endif

