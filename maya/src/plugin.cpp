

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h"

#include "shrinkwrap.h"
#include "curve_shrink_wrap.h"
#include "triangulator.h"

MStatus initializePlugin( MObject obj )
{
	
	MStatus st;
	
	MString method("initializePlugin");
	
	 MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);


	st = plugin.registerNode( "shrinkWrap", shrinkWrap::id, shrinkWrap::creator, shrinkWrap::initialize); ert;
	st = plugin.registerNode( "curveShrinkWrap", curveShrinkWrap::id, curveShrinkWrap::creator, curveShrinkWrap::initialize); ert;
	st = plugin.registerNode( "triangulator", triangulator::id, triangulator::creator, triangulator::initialize); ert;

	MGlobal::executePythonCommand("import rookery;rookery.load()");

	return st;
	
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;
	
	MString method("uninitializePlugin");
	
	MFnPlugin plugin( obj );

	st = plugin.deregisterNode( triangulator::id );ert;
	st = plugin.deregisterNode( curveShrinkWrap::id );ert;
	st = plugin.deregisterNode( shrinkWrap::id );ert;

	return st;
}



